#!/usr/bin/env bash

# Create a clean room
clean_room(){
if [ -d room ]; then
        #cd room/clean_room
        source room/clean_room/bin/activate
        pwd
else
        mkdir room && cd room
        virtualenv clean_room
        cd ..
        source room/clean_room/bin/activate
        pwd
fi
  
}

command=$(which ansible-playbook)
if [ -f dependencies ]; then
        case $1 in
        account)
                clean_room
                $command droplet.yml -e "user_command=account"
                ;;
        create)
                clean_room
                $command droplet.yml -e "user_command=create"
                ;;
        delete)
                clean_room
                $command droplet.yml -e "user_command=delete droplet_id=$2 droplet_state=present"
                ;;
        *)
                echo "Options: account | create | delete"
                ;;
        esac
else
        #Ensure pip is installed
        sudo apt-get install python-pip 
        sudo pip install --upgrade pip

        sudo pip install virtualenv
        sudo pip install -r requirements.txt
        touch dependencies
fi
